import React from 'react';
import './Navigation.css';

const Navigation = (props) => {
  return (
    <nav className="Navigation">
      <ul>
        <li onClick={props.navHandler.bind(this, 1)}><img src="dict.png" alt="" />Словарь</li><br />
        <li onClick={props.navHandler.bind(this, 2)}><img src="train.png" alt="" />Тренировка</li><br />
      </ul>
	  </nav>
  )
}

export default Navigation;
