import React, { Component } from 'react';
import './App.css';
import Navigation from '../Navigation/Navigation'
import Practise from '../Practise/Practise'
import Dictionary from '../Dictionary/Dictionary'

class App extends Component {
  shuffle(a) {
    var j, x, i, b = [...a];
    for (i = b.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = b[i];
        b[i] = b[j];
        b[j] = x;
    }
    return b;
}
  state = {
    words: [
      {eng: "Table", rus: "Стол"},
      {eng: "Fox", rus: "Лисичка"},
      {eng: "Cat", rus: "Котик"},
      {eng: "Dog", rus: "Собакен"},
      {eng: "Bridge", rus: "Мост"},
      {eng: "Hookah", rus: "Кальян"},
      {eng: "Star", rus: "Звезда"},
      {eng: "Box", rus: "Коробка"},
      {eng: "Length", rus: "Длина"},
      {eng: "Width", rus: "Ширина"},
      {eng: "Sea", rus: "Море"},
      {eng: "Sun", rus: "Солнце"},
      {eng: "Moon", rus: "Луна"},
      {eng: "Knife", rus: "Нож"},
      {eng: "Rain", rus: "Дождь"},
      {eng: "Cloud", rus: "Облако"},
      {eng: "Leaf", rus: "Лист"},
      {eng: "Car", rus: "Машина"},
      {eng: "Salt", rus: "Соль"},
      {eng: "Development", rus: "Разработка"},
    ],
    page: 1,
    started: false,
    practiseSet: [],
    userAnswers: [],
  }
  start = () => {
    let newState = {...this.state}
    newState.started = true;
    let practiseSet = this.shuffle(this.state.words).slice(0, 20);
    newState.practiseSet = practiseSet.map(word => {
        let options = this.shuffle(practiseSet).slice(0, 6).map(el => el.rus);
        if (!options.includes(word.rus)) {
          options.pop();
          options.push(word.rus);
        }
        options = this.shuffle(options);
        return {
            eng: word.eng,
            rus: word.rus,
            options: options
        }
    });
    this.setState(newState);
  }
  navHandler = (n) => {
    let newState = {...this.state}
    newState.page = n;
    this.setState(newState);
  }
  addWord = (eng, rus) => {
    eng = eng.split(' ')[0]
    rus = rus.split(' ')[0]
    const isCyrillic = text =>  /[а-я]/i.test(text);
    const isLatin    = text =>  /[a-z]/i.test(text);
    if(isCyrillic(rus) && !isLatin(rus) && !isCyrillic(eng) && isLatin(eng)) {
    let newState = {...this.state}
    newState.words.push({
      eng: eng[0].toUpperCase() + eng.substring(1), 
      rus: rus[0].toUpperCase() + rus.substring(1)})
    this.setState(newState);
    }
  }
  deleteWord = (i) => {
    let newState = {...this.state}
    newState.words.splice(i, 1)
    this.setState(newState);
  }
  chooseAnswer = (answer) => {
    let newState = {...this.state}
    newState.userAnswers.push(answer.target.innerText);
    this.setState(newState);
  }
  render() {
    return (
      <div className="App">
        <Navigation navHandler={this.navHandler} />
        <div className="wrapper">
          <Dictionary deleteWord={this.deleteWord} addWord={this.addWord} state={this.state} />
          <Practise chooseAnswer={this.chooseAnswer} start={this.start} state={this.state} />
        </div>
      </div>
    );
  }
}

export default App;
