import React from 'react';
import './Practise.css';

const Practise = (props) => {
  let active, started, wrapper, centered;
  props.state.userAnswers.length === 20 ? centered = 'centered' :  centered = "";
  props.state.page === 2 ? active = 'active' : active = '';
  props.state.started ? started = 'inactive' : started = '';
  props.state.started ? wrapper = 'active' : wrapper = '';

  let stats = React.createRef();

 let list = props.state.practiseSet.map((word, n) => {
   let active;
   n === props.state.userAnswers.length ?  active = '' :  n < props.state.userAnswers.length ? active = 'before' : active = 'after'
   return (
   <div key={"opt" + n} className={"card " + active}>
      <h2>{word.eng}</h2>
      {word.options.map(option => <div className="option" onClick={(option) => props.chooseAnswer(option)}>{option}</div>)}
    </div>
   )
  })

    let counter = 0;
    // eslint-disable-next-line
    let mistakeList = props.state.practiseSet.map((word, n) => {
      if (word.rus !== props.state.userAnswers[n]) {
        if(n < props.state.userAnswers.length) {
          return (
            <div className="output">
              <span className="l">{ word.eng}</span>
              <img alt="" className="translates_final" src="https://png.icons8.com/android/40/9b59b6/right.png" />
              <span className="r"><span className='mistake'>{props.state.userAnswers[n]}</span>{word.rus}</span>
            </div>
          )
        }
      } else {
        counter++
      }
    })


  return (
    <div className={"Practise " + active}>
      <div onClick={props.start} className={"startPractise gradient " + started}>Начать упражняться!</div>
      <div className={"PractiseWrapper " + wrapper} >
      {list}
        <div ref={stats} className={"statcard " + centered }>
          <img src="https://png.icons8.com/nolan/300/9b59b6/test-passed.png" alt="" />
          <p><span>{counter}</span> / <span >{props.state.practiseSet.length}</span></p>
          {mistakeList}
        </div>
      </div>
    </div>
  )
}

export default Practise;
