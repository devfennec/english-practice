import React from 'react';
import './Dictionary.css';

const Dictionary = (props) => {
  let eng = React.createRef();
  let rus = React.createRef();

  let wordlist = props.state.words.map((word, i) => {
    return (
      <div key={"w"+i} className="word gradient">
        <span className="eng">{word.eng}</span>
        <img alt="" className="translates" src="https://png.icons8.com/android/40/ffffff/right.png" />
        <span className="rus">{word.rus}</span>
        <span onClick={() => props.deleteWord(i)} class="delete">
          <img alt="" src="https://png.icons8.com/ios/96/ffffff/cancel.png" />
        </span>
        </div>
    )
  })

  let active;
  props.state.page === 1 ? active = 'active' : active = '';
  return (
    <div className={"Dictionary " + active}>
      <div className="wordadd word gradient">
				<div>
					<input ref={eng} type="text" placeholder="Word" />
				</div>
				<img alt="" className="translates" src="https://png.icons8.com/android/150/9b59b6/right.png" />
				<div>
					<input ref={rus} type="text" placeholder="Слово" />
				</div>
				<span onClick={() => props.addWord(eng.current.value, rus.current.value)}  className="add"><img alt="" src="https://png.icons8.com/ios/100/9b59b6/plus.png" /></span>
			</div>
      <div className="wordlist">
        {wordlist.reverse()}
      </div>
    </div>
  )
}

export default Dictionary;
