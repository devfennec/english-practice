---
# Всё переделано на React!
##### Доработки:
- Не дублируются ответы
- Минорное изменение дизайна страницы практики
- Проверка на алфавит и наличие буквенных символов



#### Запуск


```sh
$ npm start       // для отладки
$ npm run build   // Для создания билда в продакшн
```
---

